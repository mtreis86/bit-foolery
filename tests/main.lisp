(defpackage bit-foolery/tests
  (:use :cl
   :bit-foolery
        :rove))
(in-package :bit-foolery/tests)

;; NOTE: To run this test file, execute `(asdf:test-system :bit-foolery)' in your Lisp.


(deftest test-shift
  (testing "high"
    (ok (= (shift-high #b00110 5 2) #b11000))
    (ok (= (shift-high #b00110 5 3) #b10000)))
  (testing "low"
    (ok (= (shift-low #b11000 5 2) #b00110))
    (ok (= (shift-low #b00110 5 2) #b00001))))

(deftest test-rotate
  (testing "high"
    (ok (= (rotate-high #b00110 5 0) #b00110))
    (ok (= (rotate-high #b00110 5 1) #b01100))
    (ok (= (rotate-high #b00110 5 2) #b11000))
    (ok (= (rotate-high #b00110 5 3) #b10001))
    (ok (= (rotate-high #b00110 5 4) #b00011))
    (ok (= (rotate-high #b00110 5 5) #b00110)))
  (testing "low"
    (ok (= (rotate-low #b00110 5 0) #b00110))
    (ok (= (rotate-low #b00110 5 1) #b00011))
    (ok (= (rotate-low #b00110 5 2) #b10001))
    (ok (= (rotate-low #b00110 5 3) #b11000))
    (ok (= (rotate-low #b00110 5 4) #b01100))
    (ok (= (rotate-low #b00110 5 5) #b00110)))
  (testing "rotate"
    (ok (= (rotate #b00110 5 0) #b00110))
    (ok (= (rotate #b00110 5 1) #b01100))
    (ok (= (rotate #b00110 5 2) #b11000))
    (ok (= (rotate #b00110 5 3) #b10001))
    (ok (= (rotate #b00110 5 4) #b00011))
    (ok (= (rotate #b00110 5 5) #b00110))
    (ok (= (rotate #b00110 5 6) #b01100))
    (ok (= (rotate #b00110 5 -1) #b00011))
    (ok (= (rotate #b00110 5 -2) #b10001))
    (ok (= (rotate #b00110 5 -3) #b11000))
    (ok (= (rotate #b00110 5 -4) #b01100))
    (ok (= (rotate #b00110 5 -5) #b00110))
    (ok (= (rotate #b00110 5 -6) #b00011))))

(deftest test-split
  (testing "high"
    (ok (= (split-high #b1000111 3) #b1000)))
  (testing "low"
    (ok (= (split-low #b1000111 3) #b111)))
  (testing "high-from-end"
    (ok (= (split-high-from-end #b1000111 3) #b100)))
  (testing "low-from-end"
    (ok (= (split-low-from-end #b1000111 3) #b0111)))
  (testing "split"
    (multiple-value-bind (high low)
        (split #b1000111 3 1)
      (ok (= high #b100))
      (ok (= low #b111)))
    (multiple-value-bind (high low)
        (split #b1000111 1 3)
      (ok (= high #b100))
      (ok (= low #b1)))
    (multiple-value-bind (high low)
        (split #b1000111 2 0)
      (ok (= high #b10001))
      (ok (= low #b11)))))

(deftest test-select
  (testing "bit"
    (ok (= (select-bit #b1011 1) #b1))
    (ok (= (select-bit #b1011 2) #b0)))
  (testing "bit-from-end"
    (ok (= (select-bit-from-end #b1011 1) #b0))
    (ok (= (select-bit-from-end #b1011 2) #b1))))

(deftest test-flip
  (testing "bit"
    (ok (= (flip-bit #b1011 2) #b1111)))
  (testing "bit-from-end"
    (ok (= (flip-bit-from-end #b1011 2) #b1001))))

(deftest test-swap
  (testing "bits"
    (ok (= (swap-bits #b10101 1 2) #b10011)))
  (testing "bits-from-end"
    (ok (= (swap-bits-from-end #b10101 1 2 5) #b11001))))

(deftest test-mask
  (testing "low"
    (ok (= (mask-low #b010001 6 3) #b010111)))
  (testing "high"
    (ok (= (mask-high #b010001 6 3) #b111001))))

(deftest test-frame-fill
  (testing "frame-fill"
    (ok (= (frame-fill 5) #b11111))
    (ok (= (frame-fill 1) #b1))))

(deftest trim
  (testing "trim"
    (ok (= (trim #b11001010 2 4) #b00001000))
    (ok (= (trim #b11001010 1 3) #b00001010)))
  (testing "trim-to-frame"
    (ok (= (trim-to-frame #b11001010 5) #b001010))))

(deftest fold-over
  (fail "test not written"))
(deftest reverse-byte-in-int
  (fail "test not written"))
(deftest shuffle
  (fail "test not written"))
(deftest reorder
  (fail "test not written"))
(deftest macros
  (fail "test not written"))

(deftest test-frame-wise
  (testing "append-int"
    (ok (= (append-int #b110 #b101) #b101110))
    (ok (= (append-int #b0 #b10) #b100))
    (ok (= (append-int #b10 #b0) #b010))))

(deftest test-printing
  (testing "print-binary"
    (outputs (print-binary 5) "101")
    (outputs (print-binary 0) "0")
    (outputs (print-binary 1) "1")))

(deftest test-utils
  (testing "int-length"
    (ok (= (int-length #b0) 1))
    (ok (= (int-length #b1) 1))
    (ok (= (int-length #b10) 2))
    (ok (= (int-length #b11) 2))
    (ok (= (int-length #b100) 3))
    (ok (= (int-length #b101) 3))
    (ok (= (int-length #b110) 3))
    (ok (= (int-length #b0011) 2))
    (ok (= (int-length #b110110) 6))))

(deftest do-bytes
  (fail "test not written"))
(deftest do-bits
  (fail "test not written"))
(deftest do-byte-from-end
  (fail "test not written"))
(deftest do-bits-from-end
  (fail "test not written"))
