(in-package :bit-foolery)

;;;; Some tools to mess with integers as arrays of bits, trying to be low level and fast


;;; Bit-wise

(defun shift-high (int frame-width bits)
  "Shift the integer to the high in a binary window of frame-width, over bits spaces. Drops any excess.
  Should be similar to << on LE in C."
  (dpb int
       (byte (- frame-width bits) bits)
       0))

(defun shift-low (int frame-width bits)
  "Shift the integer to the low in a binary window of frame-width, over bits spaces. Drops any excess.
  Should be similar to >> on LE in C. Note, if the byte is bigger than the frame, this will shift the
  frame-width + bits portion into the frame."
  (dpb (ldb (byte (+ frame-width bits) bits) int)
       (byte frame-width 0)
       0))

(defun rotate-high (int frame-width bits)
  "Shift the integer to the high in a binary window of frame-width, over bits spaces. Wrap the excess
  back around. 0 <= bits <= frame-width."
  (let ((shift-pos (- frame-width bits)))
    (dpb (ldb (byte shift-pos 0) int)
         (byte frame-width bits)
         (ldb (byte bits shift-pos) int))))

(defun rotate-low (int frame-width bits)
  "Shift the integer to the high in a binary window of frame-width, over bits spaces. Wrap the excess
  back around. 0 <= bits <= frame-width."
  (let ((shift-pos (- frame-width bits)))
    (dpb (ldb (byte bits 0) int)
         (byte bits shift-pos)
         (ldb (byte shift-pos bits) int))))

(defun rotate (int frame-width bits)
  "Shift the integer in a binary window of frame-width, over bits spaces. Wrap the excess
  back around."
  (let* ((mod-bits (mod bits frame-width))
         (shift-pos (- frame-width mod-bits)))
    (if (zerop mod-bits)
        int
        (dpb (ldb (byte shift-pos 0) int)
             (byte frame-width mod-bits)
             (ldb (byte mod-bits shift-pos) int)))))

(defun split-high (int index)
  "Split the int into two at the index such that (split HighIndexLow) => HighIndex Low => HighIndex.
  Treats index as a little endian position.
  (split-high #b110100 2) => #b1101"
  (ldb (byte (- (int-length int) index) index) int))

(defun split-low (int index)
  "Split the int into two at the index such that (split HighIndexLow) => HighIndex Low => Low.
  Treats index as a little endian position.
  (split-low #b110100 2) => #b00"
  (ldb (byte index 0) int))

(defun split-high-from-end (int index &optional ( frame-width))
  "Split the int into two at the index such that (split HighIndexLow) => High IndexLow => High.
  (split-high-from-end #b110100 2) => #b11"
  (ldb (byte index (- (if frame-width frame-width (int-length int)) index)) int))

(defun split-low-from-end (int index &optional ( frame-width))
  "Split the int into two at the index such that (split HighIndexLow) => High IndexLow => IndexLow
  (split-low-from-end #b110100 2) => #b0100"
  (ldb (byte (- (if frame-width frame-width (int-length int)) index) 0) int))

(defun split (int index size)
  "Split the int into two values, high and low, and return them. The low will be from 0 to but
  not including index, the high will be from (+ index size) to the end of the int."
  (values (ldb (byte (- (int-length int) size index) (+ index size)) int)
          (ldb (byte index 0) int)))

(defun select-bit (int index)
  "Return only the bit at the index."
  (ldb (byte 1 index) int))

(defun select-bit-from-end (int index &optional ( frame-width))
  "Return only the bit at the index."
  (ldb (byte 1 (- (if frame-width frame-width (int-length int)) index 1)) int))

(defun flip-bit (int index)
  "Flip the bit at the index."
  (logxor int (expt 2 index)))

(defun flip-bit-from-end (int index &optional ( frame-width))
  "Flip the bit at the index."
  (logxor int (expt 2 (- (if frame-width frame-width (int-length int)) index 1))))

(defun swap-bits (int idx idy)
  "Swap the bits in positions idx and idy in the integer. Does nothing if they are equal."
  (if (plusp (logxor (select-bit int idx) (select-bit int idy)))
      (let ((frame-width (1+ (max idx idy (int-length int)))))
        (logxor int
                (shift-high 1 frame-width idx)
                (shift-high 1 frame-width idy)))
      int))

(defun swap-bits-from-end (int idx idy frame-width)
  "Swap the bits in positions idx and idy in the integer. Does nothing if they are equal."
  (if (plusp (logxor (select-bit-from-end int idx) (select-bit-from-end int idy)))
      (logxor int
              (shift-high 1 frame-width (- frame-width idx 1))
              (shift-high 1 frame-width (- frame-width idy 1)))
      int))

(defun mask-high (int frame-width bits-to-mask)
  "Return the data with bit replacing the selected number of bits from the high end with 1s."
  (trim-to-frame (dpb (frame-fill bits-to-mask)
                     (byte bits-to-mask (- frame-width bits-to-mask))
                     int)
                 frame-width))

(defun mask-low (int frame-width bits-to-mask)
  "Return the data with bit replacing the selected number of bits from the low end with 1s."
  (trim-to-frame (dpb (frame-fill bits-to-mask)
                     (byte bits-to-mask 0)
                     int)
                 frame-width))

(defun frame-fill (frame-width)
  "Return an int full of bits of given width. Equal to 2^width - 1."
  (1- (ash 1 frame-width)))
(declaim (inline frame-fill))

(defun reverse-int (int frame-width)
  "Reverses the int given the frame-width."
  (reverse-byte-in-int int 0 frame-width))

(defun trim (int start end)
  "Turn all the data before start and after end into zeros, without moving the data between within
  the int."
  (dpb (ldb (byte (- end start) start) int)
       (byte (- end start) start)
       0))

(defun trim-to-frame (int frame-width)
  "Remove all data beyond the frame."
  (ldb (byte frame-width 0) int))


;;; Byte-wise

(defun fold-over (int frame-width fold-width)
  "Reverses sets of bits in place, every other (odd) set of the given width. 01010101 folded in 2s
  is 10 01 10 01, folded in 4s is 1010 0101, etc."
    (loop with current-int = int
          for byte-index from 0 below frame-width by (* fold-width 2)
          do (setf current-int (reverse-byte-in-int current-int
                                                    (+ byte-index fold-width)
                                                    fold-width))
          finally (return-from fold-over current-int)))

(defun reverse-byte-in-int (int byte-index byte-width)
  "Considering the portion of the int as selected by the width and the index, return the int that
  has that portion reversed."
  (let ((mid-point (+ byte-index (floor byte-width 2))))
    (loop with current-int = int
          for idx from byte-index below mid-point
          for idy downfrom (1- (+ byte-index byte-width)) to mid-point
          do (setf current-int (swap-bits current-int idx idy))
          finally (return-from reverse-byte-in-int current-int))))


(defun shuffle (int frame-width byte-width)
  "Take an int and build up a new int composed of the various bytes shuffled around randomly.
  Note, this uses a list internally and will be slow on large inputs with many slices to shuffle."
  (let ((positions (loop for pos from 0 below frame-width by byte-width collecting pos)))
    (flet ((next-position ()
             (let ((next (nth (random (length positions)) positions)))
               (setf positions (remove next positions))
               next)))
      (loop with result = 0
            for index from 0 below frame-width by byte-width
            do (setf result (dpb (ldb (byte byte-width index) int)
                                 (byte byte-width (next-position))
                                 result))
            finally (return result)))))

;; Shuffle no longer takes in a position function. So this doesn't work right now.
;; (defun swap-endianness (int frame-width byte-width)
;;   "Take the int of the frame-width and return another int with the bytes swapped from LE to BE
;;   or BE to LE. This changes the order of the bytes while maintaining the order of the bits within
;;   each. Note, frame-width needs to have byte-width as a factor - pad the input."
;;   (assert (zerop (mod frame-width byte-width))) ; TODO is this really needed tho?
;;   (shuffle int frame-width byte-width 'reverse)) ; TODO there isn't a reverse fn yet to request

(defun swap-end (byte-width number)
  "Swap endianness of the number based on a byte width of byte-width. Eg, with byte-width of 8, #xAF12 -> #x12AF"
           (loop with length = (int-length number 2)
                 for position downfrom (- length byte-width) to 0 by byte-width
                 for new-num = (ldb (byte byte-width position) number)
                             then
                               (dpb (ldb (byte byte-width position) number)
                                      (byte byte-width (- length position byte-width))
                                      new-num)
                 finally (return new-num)))

(defun reorder (int frame-width byte-width sub-byte-ordering)
  "Breaking the int down into bytes of byte-width size, re-order the data within each byte according
  to the sub-byte-ordering which should be a list of byte-width size containing the numbers from
  0 below byte-width. That list will be used for shuffling each byte, from the current position
  within the byte to the new one specified. The frame needs to be evenly divided by the bytes."
  (assert (zerop (mod frame-width byte-width)))
  (loop with result = 0
        for byte-index from 0 to (- frame-width byte-width) by byte-width
        do (loop for bit-index from byte-index below (+ byte-index byte-width)
                 for next-offset in sub-byte-ordering
                 for offset = (+ next-offset byte-index)
                 do (setf result (logior result (ash (select-bit int bit-index) offset))))
        finally (return result)))


;;; Frame-wise

(defun append-int (low high)
  "Find the integer that appends, bit-wise, the two ints with low[fill-pointer] and high[0]
  being the index where they align when combined and being the last effective digit, eg pointer in
  #b0010 would be 2 so (append-int #b0010 #b1100) would be #b1110."
  (let ((length-low (int-length low))
        (length-high (int-length high)))
    (dpb high (byte length-high length-low) low)))

;;; Printing

(defun print-binary (int)
  "Print the int in binary."
  (format t "~B~%" int)
  int)

;;; Utils

(defun int-length (int &optional (radix 2))
  "How many digits are in the integer when written in binary? #b0010 is length 2."
  (assert (typep int '(or bignum fixnum)))
  (if (or (= int 0) (= int 1))
      1
      (1+ (floor (log int radix)))))

;;; Macros

(defmacro do-bytes (int symbol byte-width &body body)
  "Run body once per byte within int, specified by the byte-width, and named symbol in the loop."
  (let ((index (gensym "do-bytes-index")))
    `(loop for ,index from 0 below (int-length ,int) by ,byte-width
           for ,symbol = (ldb (byte ,byte-width ,index) ,int)
           do ,@body)))

(defmacro do-bits (int symbol &body body)
  "Run body once per bit within int, named symbol within the loop."
  (let ((index (gensym "do-bits-index")))
    `(loop for ,index from 0 below (int-length ,int)
           for ,symbol = (select-bit ,int ,index)
           do ,@body)))


(defmacro do-bytes-from-end (int symbol byte-width frame-width &body body)
  "Run body once per byte within int, specified by the byte-width, and named symbol in the loop."
  (let ((index (gensym "do-bytes-index")))
    `(loop for ,index from ,frame-width downto 0 by ,byte-width
           for ,symbol = (reverse-int (ldb (byte ,byte-width ,index) ,int) ,byte-width)
           do ,@body)))

(defmacro do-bits-from-end (int symbol frame-width &body body)
  "Run body once per bit within int, named symbol within the loop."
  (let ((index (gensym "do-bits-index")))
    `(loop for ,index from ,frame-width downto 0
           for ,symbol = (select-bit ,int ,index)
           do ,@body)))
