(cl:in-package :bfc)

(coalton-toplevel

  (declare print-bin (Integer -> Integer))
  (define (print-bin int)
    "Print the int as binary to *STANDARD-OUTPUT*."
    (progn (lisp :a (int) (cl:format cl:t "~&~B~%" int))
           (lisp :a (int) int)))

  (declare bit-extract (UFix -> UFix -> Integer -> Integer))
  (define (bit-extract size pos int)
    "Return the portion of the int at the pos of size. Wrapper for CL:LDB"
    (lisp Integer (size pos int)
      (cl:ldb (cl:byte size pos) int)))

  (declare bit-inject (Integer -> UFix -> UFix -> Integer -> Integer))
  (define (bit-inject new size pos int)
    "Deposit the bits of size from int into new at the pos. Wrapper for CL:DPB"
    (lisp Integer (new size pos int)
      (cl:dpb new (cl:byte size pos) int)))

  (declare test (Integer -> UFix))
  (define (test int)
    (the UFix (into int)))

  (declare test2 (UFix -> Integer))
  (define (test2 ufix)
    (the Integer (into ufix)))

  (declare bit-rotate (UFix -> Ifix -> Integer -> Integer))
  (define (bit-rotate frame-width bits int)
    "Shift the int within the frame-width over bits. Wrap the excess."
    ;; TODO mod currently only works on integers, this coercion wouldn't be needed if it worked on ifix
    (let ((%bits (the UFix (fromInt (mod (the Integer (into bits))
                                         (the Integer (into frame-width)))))))
      (let ((pos (- frame-width %bits)))
        (if (== %bits (fromInt 0))
            int
            (bit-inject (bit-extract pos (fromInt 0) int)
                        frame-width %bits
                        (bit-extract %bits pos int))))))

  (declare log (UFix -> Integer -> Single-Float))
  (define (log base int)
    "Return the log of the int given the base."
    (lisp Single-Float (base int) (cl:log int base)))

  (declare int-length (UFix -> Integer -> UFix))
  (define (int-length radix int)
    "Return the length of the int given the radix."
    (if (or (== int 0) (== int 1))
        (fromInt 1)
        (fromInt (+ 1 (floor (log radix int))))))

  (declare bit-length (Integer -> UFix))
  (define (bit-length int)
    "Return the minimum required number of bits to store the int."
    (int-length (fromInt 2) int))

  (declare bit-append (Integer -> Integer -> Integer))
  (define (bit-append low high)
    "Combine the integers as data, shifting high to be above low."
    (bit-inject high (bit-length high) (bit-length low) low))

  (declare bit-split (UFix -> UFix -> Integer -> (List Integer)))
  (define (bit-split index size int)
    "Split the integer at the index, returning a list of the integers, high and low. Low is from 0 to index, high is from (+ index size) to the end of the int."
    (cons (bit-extract (- (bit-length int) (- size index))
                       (+ index size)
                       int)
          (cons (bit-extract index (fromInt 0) int)
                nil)))

  (declare bit-trim (UFix -> UFix -> Integer -> Integer))
  (define (bit-trim start end int)
     "Turn all the data before start and after end into zeros, without moving the data between within the int."
    (let ((pos (- end start)))
      (bit-inject (bit-extract pos start int)
                  pos start 0))))

;; (coalton-toplevel
;;   (declare mymod ((Num :a) (Ord :a) (Num :b) (Ord :b) => (:a -> :b -> :a)))
;;   (define (mymod num base)
;;     "Compute NUM modulo BASE."
;;     (if (== base (fromInt 0))
;;         (error "Can't mod by 0.")
;;         (lisp Integer (num base) (cl:mod num base)))))












