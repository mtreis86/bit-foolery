(defpackage bit-foolery
  (:use #:cl)
  (:nicknames #:bit-foo)
  (:export
   #:shift-high
   #:shift-low
   #:rotate-high
   #:rotate-low
   #:rotate
   #:split-high
   #:split-low
   #:split-high-from-end
   #:split-low-from-end
   #:split
   #:select-bit
   #:select-bit-from-end
   #:flip-bit
   #:flip-bit-from-end
   #:swap-bits
   #:swap-bits-from-end
   #:mask-high
   #:mask-low
   #:frame-fill
   #:reverse-int
   #:trim
   #:trim-to-frame
   #:fold-over
   #:reverse-byte-in-int
   #:shuffle
   #:reorder
   ;#:swap-endianness
   #:append-int
   #:print-binary
   #:int-length
   #:do-bytes
   #:do-bits
   #:do-bytes-from-end
   #:do-bits-from-end))


(defpackage :bit-foolery-coalton
  (:use #:coalton
        #:coalton-library)
  (:nicknames #:bfc))
