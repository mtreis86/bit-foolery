(defsystem "bit-foolery"
  :version "0.1.0"
  :author "Michael Reis"
  :license "AGPL3"
  :depends-on ("coalton")
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "main")
                 (:file "coalton-versions"))))
  :description ""
  :in-order-to ((test-op (test-op "bit-foolery/tests"))))

(defsystem "bit-foolery/tests"
  :author "Michael Reis"
  :license "AGPL3"
  :depends-on ("bit-foolery"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for bit-foolery"
  :perform (test-op (op c) (symbol-call :rove :run c)))
